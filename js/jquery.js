/////////////////////////////////////////////
/////////////// scroll sticky
$(function() {
  var div_top = $("#getget").offset().top;
  var resolution = $(window).width();

  if (resolution > 1200) {
    $(window).scroll(function() {
      var window_top = $(window).scrollTop() - 0;

      if (window_top > div_top) {
        if (!$("#getget").is(".get-quote-alt")) {
          $("#getget").addClass("get-quote-alt");
        }
      } else {
        $("#getget").removeClass("get-quote-alt");
      }

      if (window_top > div_top) {
        if (!$("#getget2").is(".get-quote-alt")) {
          $("#getget2").addClass("get-quote-alt");
        }
      } else {
        $("#getget2").removeClass("get-quote-alt");
      }
    });
  }

  console.log("ready!");
});
